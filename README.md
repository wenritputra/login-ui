# Login Ui

Aplikasi Login Ui android menggunakan Expo React-Native 

## Tabel Isi

- [Tentang](#tentang)
- [Instalasi](#instalasi)

## Tentang

- [ ] Login Ui yang diawali dengan slash.
- [ ] Login Ui yang memanfaatkan React-Navigation untuk berpindah halaman.
- [ ] Login Ui form dynamis.


## Instalasi

| Add | npm | yarn |
|-- |------ | ------ |
| 1 | npm install | yarn install |
| 2 | npx expo start | yarn expo start |

