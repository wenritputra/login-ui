import React, { useEffect } from 'react';
import { View,Text,Image,StyleSheet } from 'react-native';

const HomeSplash = ({navigation}) =>{
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('HomeLogin')
        }, 3000 )
    })
    return(
        <View style= {styles.container}>
            <Image source={ require ('../image/Logo.png')} style = {{width: 60, height: 60,}}/>
            <Text style={styles.fontText}>nukar</Text>
        </View>
    )
}

export default HomeSplash;

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    fontText: {
        fontSize: 30, fontWeight: 'bold'
    }
})