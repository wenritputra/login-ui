import { useState } from 'react'
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Image, Text, View } from 'react-native';

import TextInputNumber from '../components/TextImputNumber';
import Buttons from '../components/Buttons';
import ActionBtn from '../components/ActionBtn';
 
const HomeLogin = ({navigation}) => {
    const [phone, onChangeNumber] = useState ('');

  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor='#13804C' style='light' translucent={false}/>
      <View style={styles.head}>
        <Image source={require('../image/Logo.png')} style={styles.logo}/>
        <Text style={{fontSize: 60,fontWeight: 'bold'}}>nukar</Text>
      </View>
      <View style={styles.body}>
        <Text style={{fontSize: 25,fontWeight: 'bold'}} >Sign In</Text>
        <Text>Masukan Nomor Handphone</Text>
        <TextInputNumber
          onChangeText= {text => onChangeNumber(text)}
          value= {phone}
          placeholder= 'Phone Number'
          keyboardType= 'numeric'
        />
        <Buttons 
          text= 'MASUK'
          onPress= {() => alert('Selamat Datang')}
        />
      <ActionBtn
        desc='Belum punya akun?'
        btn='DAFTAR'
        onPress={() => navigation.navigate('HomeRegister')}
      />
      </View>
    </View>
  )
}

export default HomeLogin;

const styles = StyleSheet.create({
  head: {
    height: 200,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
        // backgroundColor: '#5c5c5c'
  },
  logo: {
    width: 85, 
    height: 85,
    marginRight: 15,
  },
  body: {
    flex: 3,
    marginHorizontal: 12,
  },
})