import { useState } from 'react'
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, } from 'react-native';

import TextInputNumber from '../components/TextImputNumber';
import Buttons from '../components/Buttons';
import ActionBtn from '../components/ActionBtn';
 
const HomeRegister = ({navigation}) => {
  const [name, onChangeName] = useState ('');
  const [phone, onChangeNumber] = useState ('');
  const [email, onChangeEmail] = useState ('');
  const [code, onChangeCode] = useState ('');

  return (
    <View style={{flex: 1, backgroundColor: '#13804C'}}>
      <StatusBar backgroundColor='#13804C' style='light' translucent={false}/>
      <View style={styles.container}>
        <Text style={styles.formTitle}>Form Registration</Text>
        <TextInputNumber
          onChangeText= {text => onChangeName(text)}
          value= {name}
          placeholder= 'Full Name'
        />
        <TextInputNumber
          onChangeText= {text => onChangeNumber(text)}
          value= {phone}
          placeholder= 'Phone Number'
          keyboardType= 'numeric'
        />
        <TextInputNumber
          onChangeText= {text => onChangeEmail(text)}
          value= {email}
          placeholder= 'Email'
        />
        <TextInputNumber
          onChangeText= {text => onChangeCode(text)}
          value= {code}
          placeholder= 'Code Referal'
        />
        <Buttons 
          text= 'SIGN UP'
          onPress= {() => alert('Registration Successful')}
        />
        <ActionBtn
          desc='Sudah punya akun?'
          btn='Masuk'
          onPress={() => navigation.navigate('HomeLogin')}
      />
      </View>
    </View>
  )
}

export default HomeRegister;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffff',
    padding: 12,
    marginHorizontal: 12,
    marginTop: 20,
    borderRadius: 12
  },
  formTitle: {
    fontSize: 22,
    fontWeight: 'bold'
  },
  login: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  } 

})