import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

const Buttons = ({text, onPress}) => {
  
    return (
      <View>
         <TouchableOpacity style= {styleBtn.button} onPress={onPress}> 
          <Text style= {styleBtn.titleBtn}>{text}</Text>
        </TouchableOpacity>
      </View>

    )
}

export default Buttons;

const styleBtn = StyleSheet.create({
    button: {
        backgroundColor: '#13804C',
        marginTop: 12,
        paddingVertical: 12,
        borderRadius: 7,
        alignItems: 'center'
    },
    titleBtn: {
        fontSize: 17,
        fontWeight: 'bold',
        color: '#ffff'
    }

})