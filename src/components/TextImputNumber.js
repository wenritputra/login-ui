import { StyleSheet, View, TextInput } from 'react-native';

const TextImputNumber = props => {
    return (
      <View>
        <TextInput 
          style= {stylesInput.inputSign}
          onChangeText= {props.onChangeText}
          value= {props.value}
          placeholder= {props.placeholder}
          {...props}
          />
      </View>

    )
}

export default TextImputNumber;

const stylesInput = StyleSheet.create({
  inputSign: {
    backgroundColor:'#ffff',
    marginTop: 12,
    paddingVertical: 7,
    paddingLeft: 30, 
    borderRadius: 7,
    borderColor: '#e2e2e2',
    borderWidth: 1,
  }
})