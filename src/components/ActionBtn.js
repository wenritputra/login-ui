import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

const ActionBtn = ({desc, btn, onPress}) => {

    return (
      <View style={style.container}>
         <Text style={style.desc}>{desc}</Text>
        <TouchableOpacity onPress={onPress}>
          <Text style={style.btn}> {btn}</Text>          
        </TouchableOpacity>
      </View>
    )
}

export default ActionBtn;

const style = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
      },
    desc: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    btn: {
        color: '#E5AE01',
        fontSize: 20,
        fontWeight: 'bold'
    }
})