import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeLogin from '../pages/HomeLogin';
import HomeRegister from '../pages/HomeRegister';
import HomeSplash from '../pages/HomeSplash';

const Stack = createNativeStackNavigator();

const Router = () =>{
    return (
        <Stack.Navigator>
            <Stack.Screen
                name='HomeSplash' 
                component={HomeSplash} 
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen 
                name='HomeLogin' 
                component={HomeLogin}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen 
                name='HomeRegister' 
                component={HomeRegister}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    )
}

export default Router;
